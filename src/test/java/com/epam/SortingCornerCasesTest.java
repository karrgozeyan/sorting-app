package com.epam;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

public class SortingCornerCasesTest {
    private Sorting sorting = new Sorting();

    @Test
    public void testZeroArgument() {
        int[] expected = {};
        String[] args = {};
        int[] result = sorting.sort(args);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testOneArgument() {
        int[] expected = {2};
        String[] args = {"2"};
        int[] result = sorting.sort(args);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void testTenArguments() {
        int[] expected = {1, 2, 5, 7, 8, 11, 12, 20, 30, 40};
        String[] args = {"2", "30", "5", "1", "20", "7", "11", "12", "8", "40"};
        int[] result = sorting.sort(args);
        Assert.assertArrayEquals(expected, result);
    }

    @Test(expected = SortingException.class)
    public void testTenOrMoreArguments() {
        sorting.sort(new String[]{"1", "2", "5", "7", "2", "5", "7", "8", "11", "12", "20", "30", "40", "32", "12"});
    }
}
