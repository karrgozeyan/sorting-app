package com.epam;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingParametrizedTest {
    private Sorting sorting = new Sorting();

    private String[] input;
    private int[] expected;

    public SortingParametrizedTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"3", "4", "2"}, new int[]{2, 3, 4}},
                {new String[]{"9", "7", "8"}, new int[]{7, 8, 9}},
                {new String[]{"2", "3", "4", "2", "3", "4", "3", "4"}, new int[]{2, 2, 3, 3, 3, 4, 4, 4}},
                {new String[]{"2", "22"}, new int[]{2, 22}},
                {new String[]{"22", "2"}, new int[]{2, 22}},
                {new String[]{"5", "3", "2", "7", "1", "6", "4"}, new int[]{1, 2, 3, 4, 5, 6, 7}}
        });
    }

    @Test
    public void testSort() {
        int[] result = sorting.sort(input);
        Assert.assertArrayEquals(expected, result);
    }
}
