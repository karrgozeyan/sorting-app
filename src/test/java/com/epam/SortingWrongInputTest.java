package com.epam;

import org.junit.Test;

public class SortingWrongInputTest {
    private Sorting sorting = new Sorting();

    @Test(expected = NumberFormatException.class)
    public void testNonNumberArguments() {
        sorting.sort(new String[]{"1", "a", "2"});
    }
}
