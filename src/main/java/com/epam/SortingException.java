package com.epam;

/**
 * Custom exception for this application. Subclass of RuntimeException
 */
public class SortingException extends RuntimeException {
    /**
     * empty constructor
     */
    public SortingException() {
    }

    /**
     * Constructor with message
     */
    public SortingException(String message) {
        super(message);
    }

    /**
     * Constructor with message and cause
     */
    public SortingException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with cause
     */
    public SortingException(Throwable cause) {
        super(cause);
    }


    /**
     * Constructor with message, cause, suppression and stackTrace
     */
    public SortingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
