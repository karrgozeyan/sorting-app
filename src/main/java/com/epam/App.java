package com.epam;

/**
 * Application class
 */
public class App {
    /**
     * Main method. Receives up-to 10 arguments (integers) prints them in sorted order
     */
    public static void main(String[] args) {
        Sorting sorting = new Sorting();
        for (int num : sorting.sort(args))
            System.out.print(num + " ");
        System.out.println();
    }
}
