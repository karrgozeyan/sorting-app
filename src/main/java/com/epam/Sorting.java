package com.epam;

/**
 * Sorting class used by App
 */
public class Sorting {
    /**
     * Default constructor without arguments
     */
    public Sorting() {
    }

    /**
     * Method to create sorted integer array given array of strings
     *
     * @param args array of strings, assuming they are numbers
     * @return sorted array of integers
     */
    public int[] sort(String[] args) {
        if (args.length > 10) throw new SortingException("do not enter more than 10 arguments");
        int[] numbers = getNumbersFromArguments(args);
        quicksort(numbers, 0, args.length - 1);
        return numbers;
    }

    private void quicksort(int[] arr, int start, int end) {
        if (start >= end) return;

        int bound = partition(arr, start, end);
        quicksort(arr, start, bound - 1);
        quicksort(arr, bound + 1, end);
    }

    private int partition(int[] arr, int start, int end) {
        int bound = start - 1;
        int pivot = arr[end];

        for (int i = start; i <= end; i++) {
            if (arr[i] <= pivot) {
                bound++;

                int temp = arr[bound];
                arr[bound] = arr[i];
                arr[i] = temp;
            }
        }

        return bound;
    }

    private static int[] getNumbersFromArguments(String[] args) {
        int[] arr = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            arr[i] = Integer.parseInt(args[i]);
        }

        return arr;
    }
}
